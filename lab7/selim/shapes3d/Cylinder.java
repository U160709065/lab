package selim.shapes3d;

import selim.shapes.Circle;

public class Cylinder extends Circle {
	private int height;
	
	public Cylinder(int radius,int height){
		super(radius);
		this.height = height;
	}
	
public double area(){
	return 2 * super.area() + height * 2 ;
}

public double volume(){
	return 2 * Math.PI * getRadius()* getRadius()* height;
}
}

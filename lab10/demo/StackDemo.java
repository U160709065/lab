package demo;

import stack.Stack;
import stack.StackArrayListImpl;
import stack.StackImpl;

public class StackDemo {

	public static void main(String[] args) {
		
		
		Stack<Integer> stack = new StackArrayListImpl<>();
		stack.push(5);
		stack.push(3);
		stack.push(5);
		stack.push(9);
		
		
		Stack<Integer> stack2 = new StackArrayListImpl<>();
		stack.push(7);
		stack.push(5);
		stack.push(4);

		stack.addAll(stack2);
		
		System.out.println(stack.toList());
		
		
		int total = 0;
		while (!stack.empty()){
			System.out.println(stack.pop());
		}
			System.out.println(total);
			
		}
	}

}

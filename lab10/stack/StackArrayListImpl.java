package stack;

import java.util.ArrayList;

public class StackArrayListImpl<T> implements Stack {

	ArrayList<T> stack = new ArrayList<T>();
	
	@Override
	public void push(T item) {
		stack.add(0, item);
	}

	@Override
	public T pop() {		
		return stack.remove(0);
	}

	@Override
	public boolean empty() {
		return stack.isEmpty();
	}
	@Override
	public List<T> toList(){
		return Stack;
		
	}
	@override
	public void addAll(Stack<T> astack){
		for(int i =  aStack.toList().size() -1; i>=0; i++);{
			push(aStack.toList().get(i));
		}
	}

}

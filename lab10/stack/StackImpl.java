package stack;

import java.util.List;

public class StackImpl<T> implements Stack<T> {

	StackItem<T> top = null;
	
	@Override
	public void push(T item) {
		StackItem<T> newTop = new StackItem<T>(item);
		newTop.setNext(top);
		top = newTop;
	}

	@Override
	public T pop() {
		T item = top.getItem();
		top = top.getNext();
		return item;
	}

	@Override
	public boolean empty() {
		return (top == null);
	}
	@Override
	public List<T> toListarrayt(){
		List<T> list = new ArrayList<>();
		StackItem<T> current = top;
		while(current!=null){
			list.add(current.getItem());
			current = current.getNext();
		}
		
		return list;
	}

}

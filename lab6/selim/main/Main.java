package selim.main;
import java.util.ArrayList;

import selim.shapes.*;

public class Main {

	public static void main(String[] args) {
		
		Circle circle = new Circle(5);
		System.out.println(circle.area());

		
		Rectangle rect = new Rectangle(5,6);
		System.out.println(rect.area());
		
		ArrayList<Circle> circles = new ArrayList();
		circles.add(circle);
		circles.add(new Circle(6));
		circles.add(new Circle(7));
		
		System.out.println();
		for(int i =0;i< circles.size();i++){
			Circle circ = circles.get(i);
			System.out.println(circ.area());
			}
		
		
	}

}

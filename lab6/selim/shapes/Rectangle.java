package selim.shapes;

public class Rectangle {
	protected int sideA;
	protected int sideB;
	public Rectangle(int sideA, int sideB){
		this.sideA = sideA;
		this.sideB = sideB;
	}
	public double area(){
		return sideA * sideB;
	}
}

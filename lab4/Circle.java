public class Circle {
	double r;
public Circle(double radius){
	r = radius;
}
public double area(){
double area = r * 3.14;
return area;
}
public double perimeter(){
double perimeter = 2 * (3.14) * r;
return perimeter;
}
}

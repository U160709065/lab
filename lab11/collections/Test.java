package collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class Test {

	public static void main(String[] args) {
		String values = "23 4 6 12 5 16 12 6";
		String[] arr = values.split(" ");
		
		Collection<String> col = new ArrayList<>();
		//Test list
		for(String str: arr){
			col.add(str);
		}
		
		System.out.println(col);
		
		col.add("4");
		
		System.out.println(col);
		//Test Hashset
		Set<String> set = new HashSet<>();
		
		for(String str: arr){
			set.add(str);
		}
		System.out.println("Hashset: "+set);
		
		set.add("4");
		
		System.out.println("Hashset: "+set);
		
		//Test LinkHashset
				Set<String> Linkedset = new LinkedHashSet<>();
				
				for(String str: arr){
					set.add(str);
				}
				System.out.println("LinkedHashset: "+set);
				
				Linkedset.add("15");
				
				System.out.println("LinkedHashset: "+set);
				
				//Test TreeSet natural order
				Set<String> Treeset = new TreeSet<>();
				
				for(String str: arr){
					Treeset.add(str);
				}
				System.out.println("Treeset: "+Treeset);
				
				Treeset.add("15");
				
				System.out.println("Treeset: "+Treeset);
				
				//Test TreeSet with comparator
				Set<String> Treeset2 = new TreeSet<>(new NumberComparator());
				
				for(String str: arr){
					Treeset2.add(str);
				}
				System.out.println("Treeset Comp: "+Treeset2);
				
				Treeset2.add("15");
				
				System.out.println("Treeset comp: "+Treeset2);
				
				//Test Queue
				Queue<String>queue = new LinkedList<>();
				for(String str: arr){
					queue.add(str);
				}
				
				System.out.println("Queue: "+queue);
				
				
				
				System.out.println("removed: "+queue.remove());
				System.out.println("Queue: "+queue);
				
				//Test PriorityQueue
				Queue<String>Pqueue = new PriorityQueue<>();
				for(String str: arr){
					Pqueue.add(str);
				}
				
				System.out.println("PriorityQueue: "+Pqueue);
				
				
				
				System.out.println("removed: "+Pqueue.remove());
				System.out.println("PriorityQueue: "+Pqueue);
				
				//Test PriorityQueue
				Queue<String>Pqueue2 = new PriorityQueue<>(new NumberComparator());
				for(String str: arr){
					Pqueue.add(str);
				}
				
				System.out.println("PriorityQueue comp: "+Pqueue2);
				
				
				
				System.out.println("removed: "+Pqueue2.remove());
				System.out.println("PriorityQueue comp: "+Pqueue2);



	}

}
